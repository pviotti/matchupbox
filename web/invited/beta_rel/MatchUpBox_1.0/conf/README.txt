- ip.dat:
    leave empty for upnp/ssl client
    fill in case you manually setup port forwarding
    mypubip tcp_port udp_port mypriip ssl
- ipmap.dat
    fill only for eurecom node, otherwise leave empty
    pubip tcp_port udp_port priip
- nodes.dat
    fill it with the p2p first bootstrapper
    pubip tcp_port udp_port ssl