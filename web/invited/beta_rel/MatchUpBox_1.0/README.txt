
This Readme provides a guide to setup the Safebook network. This can be done
in three steps:
0 prerequisites setup
1 environment setup
   1.1 setup of the matryoshka bootstrappers
   1.2 setup of the TIS;
2 end-user Safebook client setup
   2.1 generating the user's credentials
   2.2 running Safebook

The TIS, the matryoshka bootstrappers and the safebook clients should be run in computers in the same LAN. The goal of the TIS is to grant certified identifiers to every Safebook node, while the goal of a matryoshka bootstrapper is allowing a Safebook newcomer to build his very first matryoshka chain. Please refer to http://www.eurecom.fr/util/popuppubli.en.htm?page=detail&id=2908 for further details.

================================================================================

0 - PREREQUISITES SETUP

To run Safebook, a user needs to dowload and install
- Python 2.6
- M2Crypto (the current version is 0.19.1)
- Twisted (the current version is 9.0.0)
- pysqlite (the current version is 2.5.6)

all the nodes running Safebook need to be in the same LAN. No firewall should block the default ports 4000UDP, nor 5000TCP.
In the pc running the TIS, no firewall should block the default port 4003UDP.

================================================================================

1 - ENVIRONMENT SETUP

=========== 1.1 - MATRYOSHKA BOOTSTRAPPERS
The goal of this section is setting up a core network of n safebook nodes that will act as matryoshka bootstrappers for every newcomer.
Please take the following steps:
- enter the ./safebook_client/coreSetup/mypem subfolder
   >> cd safebook_client/coreSetup/mypem
- generate a private/public keypair TISPKey.pem and TISKeys.pem: these will be the keys used by the Trusted Identification Service (TIS).
  >> python generateTisKeyPair.py
  To setup the TIS, please refer to the point 1.2.
  You can now get back to the coreSetup folder
  >> cd ..
- in the file adjacence.txt, write the symmetric adjacence matrix A of the n bootstrappers. A(i,j)=1 means user i is a friend of user j, while A(i,j)=0 means i and j are not friends. Please set 1 in every element of the A's diagonal.
  Leaving the default setting will setup a core of 6 nodes arranged in a chain.
- in case you add rows and columns to the adjacence matrix, please add the corresponding number of new avatar files avatarX.jpg 
- execute firstrun.py
  >> python firstrun.py adjacence.txt
  as an input, please provide the ip address of the node that will act as a bootstrapper for the p2p network in the p2p overlay. This can be set as the ip address of the first matryoshka bootstrapper.
  as an output, you will get
  - ./conf/uidX.conf  (the bootstrapper X's user id)
    ./conf/nidX.conf  (the bootstrapper X's node id)
    ./conf/nodesX.dat (the bootstrapper X's list of P2P contacts)
  - ./dbData/db[X's user ID in hex].db
  - ./mypem/[X's user ID in hex]_U_Keys.pem (pub key associated to X's user id)
    ./mypem/[X's user ID in hex]_U_PKey.pem (pri key associated to X's user id)
    ./mypem/[X's node ID in hex]_N_Keys.pem (pub key associated to X's node id)
    ./mypem/[X's node ID in hex]_N_PKey.pem (pri key associated to X's node id)
  - ./user_data/DATA_[X's user ID in hex]
  you will need to copy and paste these files in the corresponding folders of the safebook_client copy of every bootstrapper  
- copy the folder safebook_client in every bootstrapper node
- enter the safebook_client folder of boostrapper X
  >> cd safebook_client
- copy the previous uidX.conf in ./conf/uid.conf
- copy the previous nidX.conf in ./conf/nid.conf
- copy the previous nodesX.dat in ./conf/nodes.dat
- copy the previous db[X's user ID in hex].db in ./dbData/db[X's user ID in hex].db
- copy all the content of the previous mypem in ./mypem/ except the private key of the TIS! (public and private keys of nodes that are not cotacts according to the adjacence matrix are not needed)
- copy the previous DATA_[X's user ID in hex] in ./user_data/DATA_[X's user ID in hex]

=========== 1.2 - TIS SETUP
The goal of this section is setting up the Trusted Identification Service, that is responsible for issuing certificates together with unambiguous identifiers to every safebook node.
- enter the ./tis/ folder
- generate and store in the key subfolder
   - three symmetric keys: mk1, mk2, and mk3 (these files should contain a txt string)
   - a public-private keypair tis_private, tis_public
     (in case you followed step 1.1, please copy and rename the previous TISPKey.pem and TISKeys.pem in tis_private, tis_public)
- generate and store in the core folder the file core.conf containing, in the i-th row, the information
     node_id(in hex format) user_id(in hex format) ip_address
   of the i-th matryoshka bootstrapper. You can derive the nid and the uid by looking at nid.conf, uid.conf in step 1.1
- in the core/pem folder, store the public and private keys of every bootstrapper in the form
     ux(in hex format)_U_Keys.pem    (public key associated to x's user id)
     ux(in hex format)_U_PKey.pem    (private key associated to x's user id)
     nx(in hex format)_N_Keys.pem    (public key associated to x's node id)
     nx(in hex format)_N_PKey.pem    (private key associated to x's node id)
- in the conf subfolder, edit the file tis.conf only if needed.

================================================================================

2 - END-USER SAFEBOOK CLIENT SETUP

=========== 2.1 - GENERATING THE USER'S CREDENTIALS
Once the Safebook network is up, a newcomer needs a secret to be later on provided to the TIS in the account creation step. This secret is obtained out of band by face-to-face identification. To simulate this step take the following steps:
- enter the ./kgen/subfolder
- modify the line 46 of the script generatore.py and set mk as the txt string in ../tis/key/mk3  
  mk="masterkey3\n"
- run generatore.py
  >> python generatore.py  
  - please insert every information in lower case
  - gender must be "male" or "female"
  - nationality should have max 3 characters
- the script will generate a file "out"

=========== 2.2 -  running Safebook
- copy the ./safebook_client/ folder in the target client and access it
- copy there the previously generated secret "out"
- run Safebook
  >> python safebook.py
  and enter again the same information asked in 2.1  
- after the message "I HAVE BUILT MY MATRYOSHKA" it is possible to use the social network facilities
- according to your platform, edit the scripts RUNSB_X and reset_X for future use. in case of reset, step 2.2 has to be taken again.

================================================================================
