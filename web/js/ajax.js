var XMLHTTP;

function Richiesta(Pagina)
{
    if (Pagina.length > 0)
    {
        var url = Pagina;
        XMLHTTP = RicavaBrowser(CambioStato);
        XMLHTTP.open("GET", url, true);
        XMLHTTP.send(null);
        location.hash = Pagina;

    }
    else
    {
        document.getElementById("mainCont").innerHTML = "";
    } 
}

function CambioStato()
{
    if (XMLHTTP.readyState == 4)
    {
        var R = document.getElementById("mainCont");
        R.innerHTML = XMLHTTP.responseText;
    }
}

function RicavaBrowser(QualeBrowser)
{
    if (navigator.userAgent.indexOf("MSIE") != (-1))
    {
        var Classe = "Msxml2.XMLHTTP";
        if (navigator.appVersion.indexOf("MSIE 5.5") != (-1));
        {
            Classe = "Microsoft.XMLHTTP";
        } 
        try
        {
            OggettoXMLHTTP = new ActiveXObject(Classe);
            OggettoXMLHTTP.onreadystatechange = QualeBrowser;
            return OggettoXMLHTTP;
        }
        catch(e)
        {
            alert("Errore: l'ActiveX non verr� eseguito!");
        }
    }
    else if (navigator.userAgent.indexOf("Mozilla") != (-1))
    {
        OggettoXMLHTTP = new XMLHttpRequest();
        OggettoXMLHTTP.onload = QualeBrowser;
        OggettoXMLHTTP.onerror = QualeBrowser;
        return OggettoXMLHTTP;
    }
    else
    {
        alert("This browser is not supported!");
    }
}
