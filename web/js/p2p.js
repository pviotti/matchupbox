


function bootstrap() 
{
	var request = $.ajax({
		  url: "api/p2p?op=boot",
		  type: "GET",
		});

	request.done(function(msg) {
		$("#bootstrapResult").html(msg);
	});

}

function store()
{
	var request = $.ajax({
		  url: "api/p2p?op=store&key=" + $('#storeKey').val() + "&value=" + $('#storeValue').val(),
		  type: "GET",
		});

	request.done(function(msg) {
		$("#storeResult").html(msg);
	});
}

function find()
{
	var request = $.ajax({
		  url: "api/p2p?op=find&key=" + $('#findKey').val(),
		  type: "GET",
		});

	request.done(function(msg) {
		$("#findResult").html(msg);
	});
}

function dumpContacts()
{
	var request = $.ajax({
		  url: "api/p2p?op=dumpContacts",
		  type: "GET",
		});

	request.done(function(msg) {
		$("#contactsResult").html(msg);
	});
}

function dumpStorage()
{
	var request = $.ajax({
		  url: "api/p2p?op=dumpStorage",
		  type: "GET",
		});

	request.done(function(msg) {
		$("#storageResult").html(msg);
	});
}

function shutdown()
{
	var request = $.ajax({
		  url: "api/mng?op=shutdown",
		  type: "GET",
		});

	request.done(function(msg) {
		$("#shutdownResult").html(msg);
	});
}