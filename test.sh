#!/bin/bash
#----------------------------------------------------------------------------
# DATE        : 2012, November 26th
# AUTHOR      : Paolo Viotti
# DESCRIPTION : Tools for managing local MatchUpBox test instances
#----------------------------------------------------------------------------


compile() {
    mvn clean package
}

create() {
    echo "Creating $1 test instances..."
    
    rm -r test
    mkdir test

    cd test

    for i in $(seq 1 $1)
    do
        mkdir $i
        cd $i
        ln -s ../../web/ .
        ln -s ../../target/mub.jar .
        ln -s ../../log4j.properties .
        cp ../../matchupbox.properties .
        p2p_port=$(( 4600 + $i ))
        web_port=$(( 8000 + $i ))
        sed -i "s/p2p.port=.*/p2p.port=$p2p_port/g" matchupbox.properties
        sed -i "s/8000/$web_port/g" matchupbox.properties
        sed -i "s/true/false/g" matchupbox.properties
        cd ..
    done

    cd ..
}

start() {
    echo "Starting test instances..."
    
    cd test

    for i in $(ls)
    do 
        cd $i
        nohup java -jar mub.jar & >/dev/null 2>&1
        cd ..
        sleep 1
    done

    cd ..
    terminator --layout=mub --working-directory="/home/paolo/Dropbox/Development/mub-main/test"
}

kill_test() {
    kill $(ps aux | grep mub.jar | head -n -1 | tr -s " " | cut -d" " -f 2)
}


case "$1" in
    start)
        start
        ;;
    create)
        create $2
        ;;
    compile)
        compile
        ;;
    kill)
        kill_test
        ;;
    *)
        echo "Usage: $0 {start|create|compile|kill}"
esac

