MatchUpBox
==========

[MatchUpBox](http://matchupbox.com) is a comprehensive personal digital identity management solution based on a distributed architecture.  
Its code base is written in Java.  


The main driving idea of MatchUpBox development is about having a modular and agile code base which can run on many devices, 
leveraging on different toolkits and frameworks for the UI while keeping a common core business logic.  

The code is structured in "managers", each of which is in charge of a specific task such as managing the peer2peer network 
or the user interface, and so forth.  


Development tools
-----------------

These are the tools used so far for the development.  

  -  [Eclipse IDE for Java EE Developers](http://www.eclipse.org/downloads/) 
  -  [Maven](http://maven.apache.org/) as building and dependencies management system
  -  [Git](http://git-scm.com) as versioning system

Eclipse plugins used (for installing them: menu "Help"->"Eclipse Marketplace"):

  -  [Egit](http://www.eclipse.org/egit/) (see: [guide 1](http://wiki.eclipse.org/EGit/User_Guide) [guide 2](http://www.vogella.com/articles/EGit/article.html))
  -  [m2e](http://eclipse.org/m2e/)

Git guides:

  -  [Quick start guide](http://rogerdudler.github.com/git-guide/)
  -  [Git vs SVN crash course](http://git.or.cz/course/svn.html)
  -  [cheatsheet](http://ndpsoftware.com/git-cheatsheet.html)
  -  [Git official docs](http://git-scm.com/documentation)

Maven useful links:

  -  [Quick start guide](http://maven.apache.org/guides/getting-started/maven-in-five-minutes.html)
  -  [Introduction to Maven](http://maven.apache.org/guides/getting-started/index.html)
  -  [Maven tutorials](http://www.mkyong.com/tutorials/maven-tutorials/)
  -  [Maven repository search engine](http://mvnrepository.com/)


Dependencies
------------

These are the current development dependencies (as reported on the pom.xml file):

  -  [TomP2P](http://tomp2p.net/) 4.2
  -  [Jetty](http://www.eclipse.org/jetty/) 9.0
  -  [Log4J](http://logging.apache.org/log4j/1.2/) 1.2


Testing
-------

The script `test.sh` can be used to compile the desktop distribution, create some instances based on it
and run them locally.
