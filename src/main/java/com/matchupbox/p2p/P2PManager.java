package com.matchupbox.p2p;


import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ConcurrentSkipListMap;

import net.tomp2p.connection.Bindings;
import net.tomp2p.futures.FutureBootstrap;
import net.tomp2p.futures.FutureDHT;
import net.tomp2p.futures.FutureDiscover;
import net.tomp2p.p2p.Peer;
import net.tomp2p.p2p.PeerMaker;
import net.tomp2p.peers.Number160;
import net.tomp2p.peers.Number480;
import net.tomp2p.peers.PeerAddress;
import net.tomp2p.storage.Data;
import net.tomp2p.storage.StorageMemory;

import org.apache.log4j.Logger;

import com.matchupbox.Config;


public class P2PManager {
	
	private static P2PManager instance;

	private static Logger logger = Logger.getLogger(Config.LOGGER_NAME);
	private static Config confManager = Config.getInstance();
	
	private static Peer peer;
	
	private int port; 
	private PeerAddress bootstrapNode;
	
	public enum P2pStatus { CONNECTED, CONNECTING, DISCONNECTED };
	private P2pStatus status = P2pStatus.DISCONNECTED;
	

	private P2PManager() {
		port = Integer.parseInt(confManager.getProperty(Config.KEY_P2P_PORT));
		
		bootstrapNode = new PeerAddress(new Number160( confManager.getProperty(Config.KEY_P2P_BOOTSTRAP_ID)) ,
										new InetSocketAddress(confManager.getProperty(Config.KEY_P2P_BOOTSTRAP_ADD),
												Integer.parseInt(confManager.getProperty(Config.KEY_P2P_BOOTSTRAP_PORT))));
	}
	
	public static P2PManager getInstance () {
		if (instance == null)
			instance = new P2PManager();
		return instance;
	}
	
	public void connect() throws IOException {
		
		Random rnd = new Random();
		PeerMaker peerMaker;
		if ("true".equalsIgnoreCase(confManager.getProperty(Config.KEY_P2P_BOOTSTRAP_AMI))) {
			peerMaker = new PeerMaker(new Number160(confManager.getProperty(Config.KEY_P2P_BOOTSTRAP_ID)));
		} else {
			peerMaker = new PeerMaker( new Number160( rnd ) );				// TODO load id from property file
		}
		peerMaker.setStorage(new StorageMemory());							// TODO implement p2p storage
	    peerMaker.setPorts( port );
	    peerMaker.setBehindFirewallPeerMap(false); // XXX !! 
	    Bindings bindings = new Bindings();
	    bindings.addInterface("eth0");		// XXX
	    //bindings.addInterface("lo");
	    peerMaker.setBindings(bindings);
	    try {
			peer = peerMaker.makeAndListen();
		} catch (IOException e) {
			logger.error("P2PManager: Failed to connect", e);
			throw e;
		}
	}
	
	
	public boolean bootstrap() {
		
		if ("true".equalsIgnoreCase(confManager.getProperty(Config.KEY_P2P_BOOTSTRAP_AMI))) {
			logger.info("I am a bootstrap server: no need to bootstrap");
			return true;
		}
		
		//peer.setBehindFirewall(true);
		//find out how the other peers see me
		peer.getConfiguration().setBehindFirewall(false);
		FutureDiscover fd = peer.discover().setPeerAddress(bootstrapNode).start();
		fd.awaitUninterruptibly();
		if (fd.isSuccess())
			logger.info("Discovery succeded. My outside address is: " + fd.getPeerAddress());
		else
			logger.info("Discovery failed: " + fd.getFailedReason());

		FutureBootstrap futureBootstrap = peer.bootstrap().setPeerAddress( bootstrapNode ).start();
        futureBootstrap.awaitUninterruptibly();
        return futureBootstrap.isSuccess();
	}
	
	public String getMyId() {
		return peer.getPeerID().toString();
	}
	
	
	public String showContacts() {
		logger.info("Peer contact list: " + peer.getPeerBean().getPeerMap().getAll());
		return peer.getPeerBean().getPeerMap().getAll().toString();
	}
	
	public String getContatList() {
		return peer.getPeerBean().getPeerMap().getAll().toString();
	}
	
	public boolean put(int key, String value) throws IOException {
		FutureDHT futureDHT = peer.put( new Number160(key) ).setData( new Data( value ) ).start();
		futureDHT.awaitUninterruptibly();
		logger.info("Stored [key: " + (new Number160(key)) + ", value: "+ value +"]; success? " + futureDHT.isSuccess());
		return futureDHT.isSuccess();
    }
	    
	public String get(int key) throws ClassNotFoundException, IOException {
       FutureDHT futureDHT = peer.get( new Number160(key) ).start();
       futureDHT.awaitUninterruptibly();
       return (String) futureDHT.getData().getObject();
    } 
	    
	public void dumpStorage(){
      ConcurrentSkipListMap<Number480, Data> map = (ConcurrentSkipListMap<Number480, Data>) peer.getPeerBean().getStorage().map();
      for (Number480 num : map.keySet()){
    	  logger.info("[key: " + num + "; value: " + map.get(num));
      }
    }
	
	public String getStorage() {
		String resp = "";
		
		ConcurrentSkipListMap<Number480, Data> map = (ConcurrentSkipListMap<Number480, Data>) peer.getPeerBean().getStorage().map();
	      for (Number480 num : map.keySet())
	    	  resp += "[key: " + num + "; value: " + map.get(num) + "]\n";
	      
	    return resp;
	}
	
	public boolean shutdown() {
		peer.shutdown();
		return true;
	}
	
	/**
	 * XXX main for debugging purposes
	 */
	public static void main(String [] args) throws NumberFormatException, IOException, ClassNotFoundException{
		
		/*int basePort = 4000;
	    Random rnd = new Random();
	      PeerMaker peerMaker;
	      if (args[0].equals("0"))
	        peerMaker = new PeerMaker( new Number160( 12345 ) );
	      else
	        peerMaker = new PeerMaker( new Number160( rnd ) );
	      peerMaker.setBehindFirewallPeerMap(false);
	      Bindings b = new Bindings();
	      b.addInterface("lo");
	      peerMaker.setBindings(b);
	      peerMaker.setStorage(new StorageMemory());
	      peerMaker.setPorts( basePort  + Integer.parseInt(args[0]));
	      
	
	      peer = peerMaker.makeAndListen();
	      System.out.println("Set up peer " + peer.getPeerAddress());
	 
	
	      if (!args[0].equals("0")) {
	        InetSocketAddress inetSock = new InetSocketAddress("127.0.0.1", basePort);
	        FutureBootstrap futureBootstrap = peer.bootstrap().setPeerAddress( new PeerAddress(new Number160( 12345 ), inetSock.getAddress(), basePort, basePort) ).start();
	        futureBootstrap.awaitUninterruptibly();
	        System.out.println("Peer bootstrapped, success? " + futureBootstrap.isSuccess());
	        showContacts();
	      }
	      
	      Scanner input = new Scanner(System.in);
	      String command;
	      while ((command = input.nextLine()) != null) {
	        System.out.print("> ");
	        String[] cmds = command.trim().toLowerCase().split(" ");
	        if (cmds[0].equalsIgnoreCase("c"))
	          showContacts();
	        else if (cmds[0].equalsIgnoreCase("s"))
	          put(Integer.parseInt(cmds[1]), cmds[2]);
	        else if (cmds[0].equalsIgnoreCase("g"))
	          System.out.println("Got value " + get(Integer.parseInt(cmds[1])) + " for key " + cmds[1]);
	        else if (cmds[0].equalsIgnoreCase("d"))
	          dumpStorage();
	        else
	          System.out.println("Unknown command");
	      }
	      input.close();*/
	}
}
