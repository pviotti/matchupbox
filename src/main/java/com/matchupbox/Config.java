package com.matchupbox;

import java.io.FileInputStream;
import java.util.Properties;

import org.apache.log4j.PropertyConfigurator;

public class Config {

	private static Config instance;
	
	private Properties matchUpBoxProperties;
	
	private static String generalConfFileName = "matchupbox.properties";
	private static String log4jConfFileName = "log4j.properties";
	
	public static String LOGGER_NAME = "matchupboxLogger";
		
	public static String KEY_WEB_PORT = "com.matchupbox.web.port";
	
	
	public static String KEY_P2P_PORT = "com.matchupbox.p2p.port";
	
	public static String KEY_P2P_BOOTSTRAP_AMI = "com.matchupbox.p2p.bootstrapnode.ami";
	public static String KEY_P2P_BOOTSTRAP_ADD = "com.matchupbox.p2p.bootstrapnode.address";
	public static String KEY_P2P_BOOTSTRAP_PORT = "com.matchupbox.p2p.bootstrapnode.port";
	public static String KEY_P2P_BOOTSTRAP_ID = "com.matchupbox.p2p.bootstrapnode.id";
	

	private Config () {
		try {
			matchUpBoxProperties = new Properties();
			matchUpBoxProperties.load(new FileInputStream(generalConfFileName));
		
			PropertyConfigurator.configure(new FileInputStream(log4jConfFileName));
		} catch (Exception e) {
			System.err.println("ERROR: Could not find properties and/or log4j configuration files.");
			System.exit(-1);
		}
	}
	
	public static Config getInstance () {
		if (instance == null)
			instance = new Config();
		return instance;
	}
	
	public String getProperty (String key) {
		return matchUpBoxProperties.getProperty(key);
	}
}
