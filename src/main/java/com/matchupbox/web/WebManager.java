package com.matchupbox.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;

import com.matchupbox.Config;
import com.matchupbox.p2p.P2PManager;


public class WebManager extends HttpServlet {
	
	private static WebManager instance;
	
	private static final long serialVersionUID = 1L;
	
	private static Config confManager = Config.getInstance();
	private static Logger logger = Logger.getLogger(Config.LOGGER_NAME);
	protected static P2PManager p2pManager = P2PManager.getInstance();
	
	private int serverPort;
	private static Server jetty;
	
	private String API_BASE_PATH = "/api";
	
	
	public static WebManager getInstance () {
		if (instance == null)
			instance = new WebManager();
		return instance;
	}
	

	 protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			 throws ServletException, IOException {
        response.setContentType("text/html");
        
        if ("shutdown".equalsIgnoreCase(request.getParameter("op")))
        	this.stopServer();
        
        response.setStatus(HttpServletResponse.SC_OK);
        response.getWriter().println("OK, bye.");
//        response.getWriter().println("<h1>Hello Servlet, API!</h1>");
//        response.getWriter().println("session=" + request.getSession(true).getId());
	}
	 
	
	public void startServer() throws Exception {
		
		serverPort = Integer.parseInt(confManager.getProperty(Config.KEY_WEB_PORT));
		jetty = new Server(serverPort);  
	    
	    ResourceHandler resourceHandler = new ResourceHandler();
	    resourceHandler.setDirectoriesListed(true);
	    resourceHandler.setWelcomeFiles(new String[]{ "index.html" });
	    resourceHandler.setResourceBase("web");
	    
	    ServletContextHandler servletContextHandler = new ServletContextHandler(jetty, API_BASE_PATH, true, false);
//        servletContextHandler.addServlet(WebManager.class, "/hello-servlet");
      servletContextHandler.addServlet(P2PServlet.class, "/p2p");
      servletContextHandler.addServlet(WebManager.class, "/mng");

	    HandlerList handlers = new HandlerList();
	    handlers.setHandlers(new Handler[] { resourceHandler, servletContextHandler });
	    jetty.setHandler(handlers);	    
    
	    jetty.start();
	}
	
	public void stopServer() {
		try {
			//jetty.join();
			p2pManager.shutdown();
			jetty.stop();
		} catch (Exception e) {
			logger.error("Error while stopping Jetty server: ", e);
		}
	}
	
	
	public int getServerPort() { return serverPort; }
	
	 
	 /**
	  * XXX for debugging purposes
	  * @param args
	  */
	public static void main(String[] args) 
	{
	}

}
