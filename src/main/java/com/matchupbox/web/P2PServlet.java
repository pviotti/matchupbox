package com.matchupbox.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class P2PServlet extends WebManager {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			 throws ServletException, IOException {
		 
		String op = request.getParameter("op");
		
		String resp = "void";
		
		if ("boot".equalsIgnoreCase(op)) {
			if (p2pManager.bootstrap())
				resp = "bootstrapped - " + p2pManager.getMyId();
			else
				resp = "bootstrap Failed!";
		} else if ("store".equalsIgnoreCase(op)) {
			
			resp = "Stored: " + p2pManager.put(Integer.parseInt(request.getParameter("key")), request.getParameter("value"));
			
		} else if ("find".equalsIgnoreCase(op)) {
			
			try {
				resp = p2pManager.get(Integer.parseInt(request.getParameter("key")));
			} catch (Exception e) {
				resp = "An error happened: " + e.getMessage();
			} 
			
		} else if ("dumpContacts".equalsIgnoreCase(op)) {

			resp = p2pManager.showContacts();
			
		} else if ("dumpStorage".equalsIgnoreCase(op)) {
			
			resp = p2pManager.getStorage();
		}
		 
        response.setContentType("text/html");
        response.setStatus(HttpServletResponse.SC_OK);
        response.getWriter().println(resp);
	}
	
}
