package com.matchupbox.applet;


import java.awt.Graphics;

import javax.swing.JApplet;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;


public class Main extends JApplet {

	private static final long serialVersionUID = -7211483516143423841L;
	
	private StringBuffer buffer;
	private Server serv;

	// Called when this applet is loaded into the browser.
    public void init() {
    	
        // Execute a job on the event-dispatching thread; creating this applet's GUI.
        try {
//            SwingUtilities.invokeAndWait(new Runnable() {
//                public void run() {
//                    JLabel lbl = new JLabel("Hello world, I am an applet. I come in peace :)");
//                    add(lbl);
//                }
//            });
        
            buffer = new StringBuffer();
            addItem("Init()... ");
            
            System.out.println("Finishing the layout part...");
            
            

        } catch (Exception e) {
            System.err.println("createGUI didn't complete successfully");
        }
    }
    
    
    public void start() {
        addItem("Start()... ");
        
        serv = new Server();
        serv.start();
        
        showStatus("I am a status line: Server Loaded!");
    }

    public void stop() {
        addItem("Stop()... ");
        
        serv.shutdownServer();
    }

    public void destroy() {
        addItem("Destroy()...");
    }

    private void addItem(String newWord) {
        System.out.println(newWord);
        buffer.append(newWord);
        repaint();
    }
    
    
    public void paint(Graphics g) {
    	//Draw a Rectangle around the applet's display area.
            g.drawRect(0, 0, 
    		   getWidth() - 1,
    		   getHeight() - 1);

    	//Draw the current string inside the rectangle.
            g.drawString(buffer.toString(), 5, 15);
        }
}
