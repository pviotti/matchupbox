package com.matchupbox.applet;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class Server extends Thread {

	private static int serverPort = 7000;
	private ServerSocket ss;
	
	@Override
	public void run() {
		ExecutorService es = Executors.newFixedThreadPool(10);
		ss = null; Socket s = null;
		try {
			try {
				ss = new ServerSocket(serverPort);
				ss.setReuseAddress(true);
				while(true){
					s = ss.accept();
					Handler m = new Handler(s);
					es.execute(m);
				}
			} finally {
				if (ss!=null) ss.close();
				if (s!=null) s.close();
			}
		} catch (Exception e){
			System.out.println(e.getMessage());
		}
	}
	
	
	public void shutdownServer(){
		try {
			if (ss!=null) ss.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
}
