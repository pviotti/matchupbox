package com.matchupbox;

import java.awt.Desktop;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;

import com.matchupbox.p2p.P2PManager;
import com.matchupbox.web.WebManager;


public class Main {
	
	private static P2PManager p2pManager;
	private static WebManager webManager;
	
	
	/**
	 * Start up all MatchUpBox components.
	 */
	public static void startLocalInstance() {
		
		try {
			Config.getInstance();									// Load configuration properties
			Logger logger = Logger.getLogger(Config.LOGGER_NAME);	// Load logger
			
			logger.info("Starting P2PManager");
			p2pManager = P2PManager.getInstance();
			p2pManager.connect();
			/*if (p2pManager.bootstrap())
				logger.info("P2P bootstrapped successfully");
			else 
				logger.error("Could not bootstrap P2P network!");*/
			
			// Start MatryoshkaManager
			// TODO
			
			logger.info("Starting WebManager");
			webManager = WebManager.getInstance();
			webManager.startServer();
			
			
			logger.info("Opening web interface");
			Desktop.getDesktop().browse(new URI("http://localhost:" + webManager.getServerPort()));		
			
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null,	"Could not find the configuration file.", 
												"MatchUpBox: configuration error!", JOptionPane.ERROR_MESSAGE);
			System.err.println("Exception while starting MatchUpBox: " +  e.getMessage());
		} catch (IOException e) {	// URISyntaxException
			JOptionPane.showMessageDialog(null,	"An IO error happened, please check your configuration.", 
												"MatchUpBox: IO error!", JOptionPane.ERROR_MESSAGE);
			System.err.println("Exception while starting MatchUpBox: " +  e.getMessage());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,	"An error happened, please check your configuration.", 
												"MatchUpBox: generic error!", JOptionPane.ERROR_MESSAGE);
			System.err.println("Exception while starting MatchUpBox: " +  e.getMessage());
		}
	}
	
	
	public static void shutdownLocalInstance() {
		// Stop MatryoshkaManager
		// TODO
		
		// Stop P2Pmanager
		// TODO
		
		// Stop WebManager
		// TODO
	}
	

	/**
	 * Starting method for main local MatchUpBox instance 
	 */
	public static void main(String[] args) {
		Main.startLocalInstance();
	}
}
